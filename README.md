# OpenVPN bash

Simple bash-script for more simple work with OpenVPN 3.

# Usage

- Change file path.
- Use of the following commands:  
`qvpn start` - establish connection.  
`qvpn stop` - disconnect.  
`qvpn stats` - get connection statistics.  
